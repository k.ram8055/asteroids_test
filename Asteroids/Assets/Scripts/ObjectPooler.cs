﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    [System.Serializable]
    public class Pool
    {
        public GameObject obj;
        public string Tag;
        public int Size;
    }

    public List<Pool> poolList;
    public Transform bulletHolder;
    public Dictionary<string, Queue<GameObject>> poolDictionary;

    #region Singleton
    public static ObjectPooler instace;

    private void Awake()
    {
        instace = this;
    }
    #endregion

    private void Start()
    {
        poolDictionary = new Dictionary<string, Queue<GameObject>>();

        foreach(Pool pool in poolList)
        {
            Queue<GameObject> objQueue = new Queue<GameObject>();

            for (int i = 0; i < pool.Size; i++)
            {
                GameObject go = Instantiate(pool.obj, bulletHolder);
               
                go.SetActive(false);
                objQueue.Enqueue(go);
            }
            poolDictionary.Add(pool.Tag, objQueue);
        }
    }

    public GameObject SpawnfromPool(string tag,Vector3 Position, Quaternion Rotation)
    {
        if (!poolDictionary.ContainsKey(tag))
        {
            Debug.Log("Pool with tag " + tag +" doesn't exist");
            return null;
        }

        GameObject objtoSpawn = poolDictionary[tag].Dequeue();
        objtoSpawn.SetActive(true);
        objtoSpawn.transform.position = Position;
        objtoSpawn.transform.rotation = Rotation;

        IPooledObj poolobj = objtoSpawn.GetComponent<IPooledObj>();

        if (poolobj != null)
        {
            poolobj.OnSpwaned();
        }

        poolDictionary[tag].Enqueue(objtoSpawn);
        return objtoSpawn;
    }

}
