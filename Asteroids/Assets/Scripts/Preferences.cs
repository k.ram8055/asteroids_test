﻿using System;
using UnityEngine;

public class Preferences
{
    const string RESTART = "Restart";
    const string HIGHESTSCORE = "HighestSscore";
    const string VOLUME = "Volume";
    const string LEVELCOUNT = "LevelCount";

    public static void InitPrefs()
    {
        if (!PlayerPrefs.HasKey(RESTART))
        {
            PlayerPrefs.SetString(RESTART, bool.FalseString);
        }       
        if (!PlayerPrefs.HasKey(HIGHESTSCORE))
        {
            PlayerPrefs.SetInt(HIGHESTSCORE, 0);
        }    
        if (!PlayerPrefs.HasKey(LEVELCOUNT))
        {
            PlayerPrefs.SetInt(LEVELCOUNT, 0);
        }       
        if(!PlayerPrefs.HasKey(VOLUME))
        {
            PlayerPrefs.SetString(VOLUME, bool.TrueString);
        }        
    }

    public static bool Restart
    {
        get
        {
            return bool.Parse(PlayerPrefs.GetString(RESTART, bool.FalseString));
        }
        set
        {
            PlayerPrefs.SetString(RESTART, value.ToString());
        }
    }
   
    public static bool Volume
    {
        get
        {
            return bool.Parse(PlayerPrefs.GetString(VOLUME, bool.TrueString));
        }
        set
        {
            PlayerPrefs.SetString(VOLUME, value.ToString());
        }
    }
  
    public static int HighestScore
    {
        get
        {
            return PlayerPrefs.GetInt(HIGHESTSCORE, 0);
        }
        set
        {
            PlayerPrefs.SetInt(HIGHESTSCORE, value);
        }
    }

    public static int LevelCount
    {
        get
        {
            return PlayerPrefs.GetInt(LEVELCOUNT, 0);
        }
        set
        {
            PlayerPrefs.SetInt(LEVELCOUNT, value);
        }
    }
}
