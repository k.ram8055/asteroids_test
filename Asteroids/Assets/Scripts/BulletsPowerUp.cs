﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletsPowerUp : MonoBehaviour
{
    public Rigidbody rb;
    private float maxRotation;
    private float maxSpeed;

    private float rotationX;
    private float rotationY;
    private float rotationZ;

    public GameManager GM;
    public Camera mainCam;

    private void Start()
    {
        mainCam = Camera.main;
        rb = this.gameObject.GetComponent<Rigidbody>();
        GM = GameManager.instance;
        RandomMovement();
    }

    void Update()
    {
        if (GM != null && GM.CurrenGameState == GameManager.GameState.PLAY)
        {
            PowerUpMovement();
            CheckPosition();
        }
    }

    void RandomMovement()
    {
        mainCam = Camera.main;

        maxRotation = 25f;
        rotationX = Random.Range(-maxRotation, maxRotation);
        rotationY = Random.Range(-maxRotation, maxRotation);
        rotationZ = Random.Range(-maxRotation, maxRotation);

        float speedX = Random.Range(200f, 800f);
        int selectorX = Random.Range(0, 2);
        float dirX = 0;
        if (selectorX == 1)
        {
            dirX = -1;
        }
        else
        {
            dirX = 1;
        }
        float finalSpeedX = speedX * dirX;

        rb.AddForce(transform.right * finalSpeedX);

        float speedY = Random.Range(200f, 800f);
        int selectorY = Random.Range(0, 2);
        float dirY = 0;
        if (selectorY == 1)
        {
            dirY = -1;
        }
        else
        {
            dirY = 1;
        }
        float finalSpeedY = speedY * dirY;

        rb.AddForce(transform.up * finalSpeedY);
    }

    public void PowerUpMovement()
    {
        this.transform.Rotate(new Vector3(rotationX, rotationY, 0) * Time.deltaTime);
        float dynamicMaxSpeed = GM._asteroidSpeed;
        rb.velocity = new Vector2(Mathf.Clamp(rb.velocity.x, -dynamicMaxSpeed, dynamicMaxSpeed), Mathf.Clamp(rb.velocity.y, -dynamicMaxSpeed, dynamicMaxSpeed));
    }

    private void CheckPosition()
    {
        float sceneWidth = mainCam.orthographicSize * 2 * mainCam.aspect;
        float sceneHeight = mainCam.orthographicSize * 2;

        float sceneRightEdge = sceneWidth / 2;
        float sceneLeftEdge = sceneRightEdge * -1;
        float sceneTopEdge = sceneHeight / 2;
        float sceneBottomEdge = sceneTopEdge * -1;

        if (transform.position.x > sceneRightEdge)
        {
            transform.position = new Vector2(sceneLeftEdge, transform.position.y);
        }
        if (transform.position.x < sceneLeftEdge)
        {
            transform.position = new Vector2(sceneRightEdge, transform.position.y);
        }
        if (transform.position.y > sceneTopEdge)
        {
            transform.position = new Vector2(transform.position.x, sceneBottomEdge);
        }
        if (transform.position.y < sceneBottomEdge)
        {
            transform.position = new Vector2(transform.position.x, sceneTopEdge);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            GM.Powerup_SFX.Play();

            this.gameObject.SetActive(false);
            GM._bulletPowerUp = true;
            Destroy(Instantiate(GM.PS_BulletPowerUp, this.transform.position, this.transform.rotation), 2f);

            // GM.Player.GetComponent<PlayerController>().bull.SetActive(true);
        }
    }

}
