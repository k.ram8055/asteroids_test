﻿/* 
 * Game         : Asteroids_Test
 * Developer    : Ram Katighar
 * Description  : This scripts handles the Asteroid movement and their behaviour
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidAI : MonoBehaviour
{
    public GameObject Asteroid;
    private float maxRotation;
    private float maxSpeed;

    private float rotationX;
    private float rotationY;
    private float rotationZ;

    private Rigidbody rb;
    private Camera mainCam;

    private int _generation;
    private bool allAsteroidsOffScreen;
    private GameManager GM;

    void Start()
    {
        GM = GameManager.instance;
        RandomMovement();
    }

    // This method is used to move and rotate the Asteroid randomly
    void RandomMovement()
    {
        mainCam = Camera.main;

        maxRotation = 25f;
        rotationX = Random.Range(-maxRotation, maxRotation);
        rotationY = Random.Range(-maxRotation, maxRotation);
        rotationZ = Random.Range(-maxRotation, maxRotation);

        rb = Asteroid.GetComponent<Rigidbody>();

        float speedX = Random.Range(200f, 800f);
        int selectorX = Random.Range(0, 2);
        float dirX = 0;
        if (selectorX == 1)
        {
            dirX = -1;
        }
        else
        {
            dirX = 1;
        }
        float finalSpeedX = speedX * dirX;

        rb.AddForce(transform.right * finalSpeedX);

        float speedY = Random.Range(200f, 800f);
        int selectorY = Random.Range(0, 2);
        float dirY = 0;
        if (selectorY == 1)
        {
            dirY = -1;
        }
        else
        {
            dirY = 1;
        }
        float finalSpeedY = speedY * dirY;

        rb.AddForce(transform.up * finalSpeedY);
    }

    void Update()
    {
        if (GM != null && GM.CurrenGameState == GameManager.GameState.PLAY)
        {
            AsteroidMovement();
            CheckPosition();
        }
    }

    public void AsteroidMovement()
    {
        Asteroid.transform.Rotate(new Vector3(rotationX, rotationY, 0) * Time.deltaTime);
        float dynamicMaxSpeed = 3f;
        rb.velocity = new Vector2(Mathf.Clamp(rb.velocity.x, -dynamicMaxSpeed, dynamicMaxSpeed), Mathf.Clamp(rb.velocity.y, -dynamicMaxSpeed, dynamicMaxSpeed));
    }

    //This method checks the position of Asteroid if it crosses the cameraview and adjust it's postion
    #region CHECKPOSTION
    private void CheckPosition()
    {
        float sceneWidth = mainCam.orthographicSize * 2 * mainCam.aspect;
        float sceneHeight = mainCam.orthographicSize * 2;
        float sceneRightEdge = sceneWidth / 2;
        float sceneLeftEdge = sceneRightEdge * -1;
        float sceneTopEdge = sceneHeight / 2;
        float sceneBottomEdge = sceneTopEdge * -1;

        float asteroidOffset;
        if (allAsteroidsOffScreen)
        {
            asteroidOffset = 1.0f;
            float reverseSpeed = 2000.1f;

            if (Asteroid.transform.position.x > sceneRightEdge + asteroidOffset)
            {
                Asteroid.transform.rotation = Quaternion.identity;
                rb.AddForce(transform.right * (reverseSpeed * (-1)));
            }

            if (Asteroid.transform.position.x < sceneLeftEdge - asteroidOffset)
            {
                Asteroid.transform.rotation = Quaternion.identity;
                rb.AddForce(transform.right * reverseSpeed);
            }
            if (Asteroid.transform.position.y > sceneTopEdge + asteroidOffset)
            {
                Asteroid.transform.rotation = Quaternion.identity;
                rb.AddForce(transform.up * (reverseSpeed * (-1)));
            }

            if (Asteroid.transform.position.y < sceneBottomEdge - asteroidOffset)
            {
                Asteroid.transform.rotation = Quaternion.identity;
                rb.AddForce(transform.up * reverseSpeed);
            }
        }
        else
        {
            asteroidOffset = 2.0f;

            if (Asteroid.transform.position.x > sceneRightEdge + asteroidOffset)
            {
                Asteroid.transform.position = new Vector2(sceneLeftEdge - asteroidOffset, Asteroid.transform.position.y);
            }

            if (Asteroid.transform.position.x < sceneLeftEdge - asteroidOffset)
            {
                Asteroid.transform.position = new Vector2(sceneRightEdge + asteroidOffset, Asteroid.transform.position.y);
            }

            if (Asteroid.transform.position.y > sceneTopEdge + asteroidOffset)
            {
                Asteroid.transform.position = new Vector2(Asteroid.transform.position.x, sceneBottomEdge - asteroidOffset);
            }

            if (Asteroid.transform.position.y < sceneBottomEdge - asteroidOffset)
            {
                Asteroid.transform.position = new Vector2(Asteroid.transform.position.x, sceneTopEdge + asteroidOffset);
            }
        }
    }
    #endregion


    public void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.name == "Bullet(Clone)")
        {
            //Using switch statement to detect tag of the this asteroid and generatting the smaller asteroids according to that size
            switch (this.gameObject.tag)
            {
                case "Size_5":
                    _CreateAsteroid(2, 3);
                    break;
                case "Size_4":
                    _CreateAsteroid(2, 2);
                    break;
                case "Size_3":
                    _CreateAsteroid(2, 1);
                    break;
                case "Size_2":
                    _CreateAsteroid(2, 0);
                    break;
                default:
                    Debug.Log("Done all sizes");
                    break;
            }

            collision.gameObject.SetActive(false);
            this.gameObject.SetActive(false);
        }
    }

    public void _CreateAsteroid(int numofasteroids, int size)
    {
        for (int i = 0; i < numofasteroids; i++)
        {
            Instantiate(GM.Asteroid[size], transform.position, Quaternion.identity, GM.AsteroidParent.transform);
        }
    }
}
