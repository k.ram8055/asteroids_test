﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour //, IPooledObj
{
  //private PlayerController pc;

    void Start()
    {
        this.gameObject.GetComponent<Rigidbody>().AddForce(transform.up * 400);
        //pc = FindObjectOfType<PlayerController>();
    }

    public void KillOldBullet()
    {
        Destroy(gameObject, 2.0f);
    }

    void OnCollisionEnter(Collision collisionInfo)
    {
        Destroy(gameObject, 0.0f);
    }

    /*  Pooling needs to fix some issues
    private void OnEnable()
    {
        Invoke("offBullet", 3f);
    }

    private void OnDisable()
    {
        CancelInvoke();
    }
    public void offBullet()
    {
        this.gameObject.SetActive(false);
    }

    public void OnSpwaned()
    {
        this.gameObject.GetComponent<Rigidbody>().AddForce(transform.up * 700);//, ForceMode.Impulse);
    }
    */
}
