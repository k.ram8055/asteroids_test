﻿/* 
 * Game         : Asteroids_Test
 * Developer    : Ram Katighar
 * Description  : This scripts handles the GamePlay,Levelmanagement, Particleffects, Audio and UI management
 */


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [Serializable]
    public class Level
    {
        public int Totalasteroid;
        public int AsteroidSpeed;
        public int TargetScore;
        public int ShieldTimer;
        public int BulletsPowerUpTimer;
    }

    public enum GameState
    {
        MENU,
        PLAY,
        FAIL,
        WIN,
        PAUSE,
    }
    public GameState CurrenGameState;

    public List<Level> Level_list;
    public List<GameObject> Generated_Asteroids;
    public List<GameObject> Asteroid;

    public GameObject Player;
    public GameObject AsteroidParent;

    public GameObject ShieldPowerUp;
    public GameObject BulletPowerUp;

    [Header("Panel's")]
    public GameObject MenuPanel;
    public GameObject InGamePanel;
    public GameObject PausePanel;
    public GameObject VictoryPanel;
    public GameObject GameOverPanel;

    public Text[] Scoretxt;
    public Text[] HighScoreText;

    public Text TargetScoreTxt;
    public Text LevelTxt;
    public Text LevelTxt_Win;
    public Text ShieldPowerUp_txt;
    public Text BulletsPowerUp_txt;

    [Header("Audio's")]
    public AudioSource Bg_Music;
    public AudioSource Bullet_SFX;
    public AudioSource Win_SFX;
    public AudioSource Death_SFX;
    public AudioSource Powerup_SFX;
    public AudioSource Tap_SFX;
    public AudioSource AsteroidBreak_SFX;

    [Header("ParticleEffects")]
    public GameObject PS_ShieldPowerUp;
    public GameObject PS_BulletPowerUp;
    public GameObject PS_AsteroidDestroy;
    public GameObject PS_RocketDestroy;

    [HideInInspector] public int _level;
    [HideInInspector] public int _score_c;
    [HideInInspector] public int _score;
    [HideInInspector] public int _targetscore;
    [HideInInspector] public int _temptarget_Score;

    [HideInInspector] public float _asteroidSpeed;
    [HideInInspector] public float _timerShield;
    [HideInInspector] public float _timerBullets;

    [HideInInspector] public bool _shieldPowerUp; 
    [HideInInspector] public bool _bulletPowerUp;

    public static GameManager instance;
   
    private void FixedUpdate()
    {
        if(CurrenGameState == GameState.PLAY)
        {
            if (_shieldPowerUp)
            {
                _timerShield -= Time.deltaTime;
                int temp = (int)_timerShield;
                ShieldPowerUp_txt.text = temp.ToString();

                if(_timerShield <= 0.0f)
                {
                    _shieldPowerUp = false;
                    Player.GetComponent<PlayerController>().ShieldPowerUp.SetActive(false);
                }
            }
            if (_bulletPowerUp)
            {
                _timerBullets -= Time.deltaTime;
                int temp1 = (int)_timerBullets;
                BulletsPowerUp_txt.text = temp1.ToString();

                if (_timerBullets <= 0.0f)
                {
                    _bulletPowerUp = false;
                }
            }
        }
    }

    
    private void Start()
    {
        instance = this;
        _level = 0;
        _targetscore = Level_list[_level].TargetScore; ;
        _temptarget_Score = _targetscore;
        _asteroidSpeed = 1f;

        if (Preferences.Restart == true)
        {            
            OnClick_Play();
        }
        else
        {
            CurrenGameState = GameState.MENU;
        }
    }
 
    public void OnClick_Play()
    {
        CurrenGameState = GameState.PLAY;

        CreateAsteroid(Level_list[_level].Totalasteroid);

        int Level_Name = _level + 1;
        LevelTxt.text = "LEVEL " + Level_Name.ToString();
        Scoretxt[0].text = "0";
        HighScoreText[0].text = Preferences.HighestScore.ToString();

        Player.SetActive(true);
        MenuPanel.SetActive(false);
        InGamePanel.SetActive(true);
        Preferences.Restart = false;
        
        StartCoroutine(Shieldoff());

    }
    IEnumerator Shieldoff()
    {
        yield return new WaitForSeconds(2f);
        if(!_shieldPowerUp)
        Player.GetComponent<PlayerController>().ShieldPowerUp.SetActive(false);
    }

    public void OnClick_Pause()
    {
        CurrenGameState = GameState.PAUSE;

        InGamePanel.SetActive(false);
        PausePanel.SetActive(true);
    }

    public void OnClick_Resume()
    {
        CurrenGameState = GameState.PLAY;

        InGamePanel.SetActive(true);
        PausePanel.SetActive(false);
    }

    public void OnClick_Menu()
    {
        SceneManager.LoadScene(0);
    }

    public void OnClick_Restart()
    {
        Preferences.Restart = true;
        SceneManager.LoadScene(0);
    }

    public void OnClick_NextLevel()
    {
        Player.GetComponent<PlayerController>().ShieldPowerUp.SetActive(true);
        InGamePanel.SetActive(true);
        VictoryPanel.SetActive(false);

        DestroyGeneratedAsteroid();
        Player.GetComponent<PlayerController>().ResetPlayerPosition();

        _level++;
        int Level_Name = _level + 1;
        LevelTxt.text = "LEVEL " + Level_Name.ToString();

        _targetscore = Level_list[_level].TargetScore; 
        TargetScoreTxt.text = _targetscore.ToString();
        _temptarget_Score = _targetscore;   

        CreateAsteroid(Level_list[_level].Totalasteroid);
        _asteroidSpeed = Level_list[_level].AsteroidSpeed;

        if(_level > 1)
        {
           GameObject sp = Instantiate(ShieldPowerUp, new Vector3(UnityEngine.Random.Range(-13, 13), 5f), transform.rotation, AsteroidParent.transform);
           GameObject bp = Instantiate(BulletPowerUp, new Vector3(UnityEngine.Random.Range(-13, 13), 5f), transform.rotation, AsteroidParent.transform);
            Generated_Asteroids.Add(sp);
            Generated_Asteroids.Add(bp);
        }

        _timerShield = Level_list[_level].ShieldTimer;
        _timerBullets = Level_list[_level].BulletsPowerUpTimer;

        CurrenGameState = GameState.PLAY;
        StartCoroutine(Shieldoff());

    }
    public void DestroyGeneratedAsteroid()
    {
        foreach(GameObject go in Generated_Asteroids)
        {
            go.SetActive(false);
        }
        Generated_Asteroids.Clear();
    }

    public void CreateAsteroid(int numofasteroids)
    {
        StartCoroutine(e_CreateAsteroid(numofasteroids));
    }

    IEnumerator e_CreateAsteroid(int numofasteroids)
    {
        for(int i = 0; i < numofasteroids; i++)
        {
            GameObject Asteroid_c =  Instantiate(Asteroid[UnityEngine.Random.Range(2,5)], new Vector3(UnityEngine.Random.Range(-13,13),5f), transform.rotation, AsteroidParent.transform);
            Generated_Asteroids.Add(Asteroid_c);
            yield return new WaitForSeconds(0.4f);
        }
    }

    public void ScoreTextUpdate()
    {
        foreach(Text txt in Scoretxt)
        {
            txt.text = _score.ToString();
        }
        foreach (Text txt in HighScoreText)
        {
            txt.text = Preferences.HighestScore.ToString();
        }
    }

    public void GameOver()
    {
        CurrenGameState = GameState.FAIL;
        DestroyGeneratedAsteroid();
        if (_score > Preferences.HighestScore)
        {
            Preferences.HighestScore = _score;
            ScoreTextUpdate();
        }
       
        InGamePanel.SetActive(false);
        GameOverPanel.SetActive(true);
    }

    public void Win()
    {
        CurrenGameState = GameState.WIN;
        _timerBullets = 0;
        _timerShield = 0;
        _bulletPowerUp = false;
        _shieldPowerUp = false;
        DestroyGeneratedAsteroid();
        int LevelNumTxt = _level + 1;
        LevelTxt_Win.text = "Completed Level " + LevelNumTxt.ToString();

        if (_score > Preferences.HighestScore)
        {
            Preferences.HighestScore = _score;
            ScoreTextUpdate();
        }
        InGamePanel.SetActive(false);
        VictoryPanel.SetActive(true);
    }


    public void LevelGeneration()
    {

    }
 
}
