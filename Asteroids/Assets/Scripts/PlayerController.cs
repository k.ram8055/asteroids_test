﻿/* 
 * Game         : Asteroids_Test
 * Developer    : Ram Katighar
 * Description  : This scripts handles the player movement and firing states
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Rigidbody rb;

    //You can adjust the below values in the inspector
    public float thrust;
    public float rotationspeed;
    public float maxmovespeed;

    public Camera mainCam;
    public GameObject _Bullet;

    public Transform Bullet_Position1;
    public Transform Bullet_Position2;
    public Transform Bullet_Position3;

    public GameObject ShieldPowerUp;
    public GameObject BulletsPowerUp;
    public GameObject PS_thrust;
    //private ObjectPooler objP;
    private GameManager GM;


    void Start()
    {
        GM = FindObjectOfType<GameManager>();

        // objP = ObjectPooler.instace;
    }

    private void FixedUpdate()
    {
        if (GM != null && GM.CurrenGameState == GameManager.GameState.PLAY)
        {
            CheckPosition();
            PlayerControl();
        }
        else if( GM == null)
        {
            Debug.Log("Null");
        }
    }

    void PlayerControl()
    {
        if (Input.GetMouseButtonDown(0) || Input.GetKeyDown("space"))
        {
            Fire();
        }

        RenderSettings.skybox.SetFloat("_Rotation", Time.time * 1f); // To rotate the Skybox

        rb.AddForce(transform.up * thrust * Input.GetAxis("Vertical"));
        rb.velocity = new Vector2(Mathf.Clamp(rb.velocity.x, -maxmovespeed, maxmovespeed), Mathf.Clamp(rb.velocity.y, -maxmovespeed, maxmovespeed));
        transform.Rotate(0, 0, Input.GetAxis("Horizontal") * rotationspeed * Time.deltaTime);
    }

    //This method checks the position of player if it crosses the cameraview and adjust the player postion
    private void CheckPosition()
    {
        float sceneWidth = mainCam.orthographicSize * 2 * mainCam.aspect;
        float sceneHeight = mainCam.orthographicSize * 2;

        float sceneRightEdge = sceneWidth / 2;
        float sceneLeftEdge = sceneRightEdge * -1;
        float sceneTopEdge = sceneHeight / 2;
        float sceneBottomEdge = sceneTopEdge * -1;

        if (transform.position.x > sceneRightEdge)
        {
            transform.position = new Vector2(sceneLeftEdge, transform.position.y);
        }
        if (transform.position.x < sceneLeftEdge)
        {
            transform.position = new Vector2(sceneRightEdge, transform.position.y);
        }
        if (transform.position.y > sceneTopEdge)
        {
            transform.position = new Vector2(transform.position.x, sceneBottomEdge);
        }
        if (transform.position.y < sceneBottomEdge)
        {
            transform.position = new Vector2(transform.position.x, sceneTopEdge);
        }
    }

    public void ResetPlayerPosition()
    {     
        transform.position = new Vector2(0f, 0f);
        transform.eulerAngles = new Vector3(0, 0, 0);
        rb.velocity = new Vector3(0f, 0f, 0f);
        rb.angularVelocity = new Vector3(0f, 0f, 0f);
    }

    void Fire()
    {
        GM.Bullet_SFX.Play();
        GameObject BulletClone = Instantiate(_Bullet, new Vector2(Bullet_Position1.position.x, Bullet_Position1.position.y), transform.rotation);
        if (GM._bulletPowerUp)
        {
            GameObject BulletClone1 = Instantiate(_Bullet, new Vector2(Bullet_Position2.position.x, Bullet_Position2.position.y), Bullet_Position2.rotation);
            GameObject BulletClone2 = Instantiate(_Bullet, new Vector2(Bullet_Position3.position.x, Bullet_Position3.position.y), Bullet_Position3.rotation);
        }
        BulletClone.SetActive(true);
        BulletClone.GetComponent<Bullet>().KillOldBullet();
        BulletClone.GetComponent<Rigidbody>().AddForce(transform.up * 200);

        //Pooling needs to fix some issues
        //objP.SpawnfromPool("Bullet", this.transform.position, this.transform.rotation);
    }

    //public void OnDisable()
    //{
    //    Destroy(Instantiate(GM.PS_RocketDestroy, this.transform.position,this.transform.rotation), 2f);
    //}
}
